local S = minetest.get_translator("zr_water");

zr_bucket.register_liquid(
	"zr_water:source",
	"zr_water:flowing",
	"zr_water:bucket",
	"zr_water_bucket.png",
	S("Water Bucket"),
	{tool = 1, water_bucket = 1}
)
minetest.register_alias("water:bucket", "zr_water:bucket")

-- River water source is 'liquid_renewable = false' to avoid horizontal spread
-- of water sources in sloping rivers that can cause water to overflow
-- riverbanks and cause floods.
-- River water source is instead made renewable by the 'force renew' option
-- used here.

zr_bucket.register_liquid(
	"zr_water:river_source",
	"zr_water:river_water_flowing",
	"zr_water:bucket_river",
	"zr_water_river_bucket.png",
	S("River Water Bucket"),
	{tool = 1, water_bucket = 1},
	true
)
minetest.register_alias("water:bucket_river", "zr_water:bucket_river")

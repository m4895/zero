zr_dungeon_loot = {}

zr_dungeon_loot.CHESTS_MIN = 0 -- not necessarily in a single dungeon
zr_dungeon_loot.CHESTS_MAX = 2
zr_dungeon_loot.STACKS_PER_CHEST_MAX = 8

dofile(minetest.get_modpath("zr_dungeon_loot") .. "/loot.lua")
dofile(minetest.get_modpath("zr_dungeon_loot") .. "/mapgen.lua")

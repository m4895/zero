local S = minetest.get_translator("zr_vessel")

minetest.register_node("zr_vessel:glass_bottle", {
	description = S("Empty Glass Bottle"),
	drawtype = "plantlike",
	tiles = {"zr_vessel_glass_bottle.png"},
	inventory_image = "zr_vessel_glass_bottle.png",
	wield_image = "zr_vessel_glass_bottle.png",
	paramtype = "light",
	is_ground_content = false,
	walkable = false,
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.3, 0.25},
	},
	groups = {vessel = 1, dig_immediate = 3, attached_node = 1},
	sounds = zr_glass.sounds,
})
minetest.register_alias("vessel:glass_bottle", "zr_vessel:glass_bottle")
minetest.register_alias("glass_bottle", "zr_vessel:glass_bottle")

minetest.register_craft( {
	output = "zr_vessel:glass_bottle 10",
	recipe = {
		{"zr_glass:glass", "", "zr_glass:glass"},
		{"zr_glass:glass", "", "zr_glass:glass"},
		{"", "zr_glass:glass", ""}
	}
})

minetest.register_node("zr_vessel:drinking_glass", {
	description = S("Empty Drinking Glass"),
	drawtype = "plantlike",
	tiles = {"zr_vessel_drinking_glass.png"},
	inventory_image = "zr_vessel_drinking_glass_inv.png",
	wield_image = "zr_vessel_drinking_glass.png",
	paramtype = "light",
	is_ground_content = false,
	walkable = false,
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.3, 0.25},
	},
	groups = {vessel = 1, dig_immediate = 3, attached_node = 1},
	sounds = zr_glass.sounds,
})
minetest.register_alias("vessel:drinking_glass", "zr_vessel:drinking_glass")
minetest.register_alias("drinking_glass", "zr_vessel:drinking_glass")

minetest.register_craft( {
	output = "zr_vessel:drinking_glass 14",
	recipe = {
		{"zr_glass:glass", "", "zr_glass:glass"},
		{"zr_glass:glass", "", "zr_glass:glass"},
		{"zr_glass:glass", "zr_glass:glass", "zr_glass:glass"}
	}
})

minetest.register_craft( {
	type = "shapeless",
	output = "zr_vessel:glass_fragments",
	recipe = {
		"zr_vessel:glass_bottle",
		"zr_vessel:glass_bottle",
	},
})

minetest.register_craft( {
	type = "shapeless",
	output = "zr_vessel:glass_fragments",
	recipe = {
		"zr_vessel:drinking_glass",
		"zr_vessel:drinking_glass",
	},
})

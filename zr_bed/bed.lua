-- beds"/zr_bed.lua

-- support for MT game translation.
local S = zr_bed.get_translator

-- Fancy shaped bed

zr_bed.register_bed("zr_bed:fancy", {
	description = S("Fancy Bed"),
	inventory_image = "zr_bed_fancy.png",
	wield_image = "zr_bed_fancy.png",
	tiles = {
		bottom = {
			"zr_bed_top1.png",
			"zr_bed_under.png",
			"zr_bed_side1.png",
			"zr_bed_side1.png^[transformFX",
			"zr_bed_foot.png",
			"zr_bed_foot.png",
		},
		top = {
			"zr_bed_top2.png",
			"zr_bed_under.png",
			"zr_bed_side2.png",
			"zr_bed_side2.png^[transformFX",
			"zr_bed_head.png",
			"zr_bed_head.png",
		}
	},
	nodebox = {
		bottom = {
			{-0.5, -0.5, -0.5, -0.375, -0.065, -0.4375},
			{0.375, -0.5, -0.5, 0.5, -0.065, -0.4375},
			{-0.5, -0.375, -0.5, 0.5, -0.125, -0.4375},
			{-0.5, -0.375, -0.5, -0.4375, -0.125, 0.5},
			{0.4375, -0.375, -0.5, 0.5, -0.125, 0.5},
			{-0.4375, -0.3125, -0.4375, 0.4375, -0.0625, 0.5},
		},
		top = {
			{-0.5, -0.5, 0.4375, -0.375, 0.1875, 0.5},
			{0.375, -0.5, 0.4375, 0.5, 0.1875, 0.5},
			{-0.5, 0, 0.4375, 0.5, 0.125, 0.5},
			{-0.5, -0.375, 0.4375, 0.5, -0.125, 0.5},
			{-0.5, -0.375, -0.5, -0.4375, -0.125, 0.5},
			{0.4375, -0.375, -0.5, 0.5, -0.125, 0.5},
			{-0.4375, -0.3125, -0.5, 0.4375, -0.0625, 0.4375},
		}
	},
	selectionbox = {-0.5, -0.5, -0.5, 0.5, 0.06, 1.5},
	recipe = {
		{"", "", "group:stick"},
		{"zr_wool:white", "zr_wool:white", "zr_wool:white"},
		{"group:wood", "group:wood", "group:wood"},
	},
})
minetest.register_alias("bed:fancy", "zr_bed:fancy_bottom")

-- Simple shaped bed

zr_bed.register_bed("zr_bed:bed", {
	description = S("Simple Bed"),
	inventory_image = "zr_bed.png",
	wield_image = "zr_bed.png",
	tiles = {
		bottom = {
			"zr_bed_top_bottom.png^[transformR90",
			"zr_bed_under.png",
			"zr_bed_side_bottom_r.png",
			"zr_bed_side_bottom_r.png^[transformfx",
			"zr_bed_transparent.png",
			"zr_bed_side_bottom.png"
		},
		top = {
			"zr_bed_top_top.png^[transformR90",
			"zr_bed_under.png",
			"zr_bed_side_top_r.png",
			"zr_bed_side_top_r.png^[transformfx",
			"zr_bed_side_top.png",
			"zr_bed_transparent.png",
		}
	},
	nodebox = {
		bottom = {-0.5, -0.5, -0.5, 0.5, 0.0625, 0.5},
		top = {-0.5, -0.5, -0.5, 0.5, 0.0625, 0.5},
	},
	selectionbox = {-0.5, -0.5, -0.5, 0.5, 0.0625, 1.5},
	recipe = {
		{"zr_wool:white", "zr_wool:white", "zr_wool:white"},
		{"group:wood", "group:wood", "group:wood"}
	},
})
minetest.register_alias("bed:bed", "zr_bed:bed_bottom")
minetest.register_alias("bed", "zr_bed:bed_bottom")

-- Fuel
minetest.register_craft({
	type = "fuel",
	recipe = "zr_bed:fancy_bottom",
	burntime = 13,
})

minetest.register_craft({
	type = "fuel",
	recipe = "zr_bed:bed_bottom",
	burntime = 12,
})


local S = minetest.get_translator("zr_lime")

zr_fence.register_wall("zr_lime:cobble_wall", {
	description = S("Cobblestone Wall"),
	texture = "zr_lime_cobble.png",
	material = "zr_lime:cobble",
})

zr_fence.register_wall("zr_lime:mossycobble_wall", {
	description = S("Mossy Cobblestone Wall"),
	texture = "zr_lime_mossycobble.png",
	material = "zr_lime:mossycobble",
})

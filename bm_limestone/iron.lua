
local S = minetest.get_translator("zr_iron");

minetest.register_node("bm_limestone:iron_ore", {
	description = S("Iron Ore"),
	tiles = {"zr_lime_stone.png^zr_iron_mineral.png"},
	groups = {cracky = 2},
	drop = "zr_iron:lump",
	sounds = zr_stone.sounds,
})

minetest.register_ore({
	ore_type       = "scatter",
	ore            = "bm_limestone:iron_ore",
	wherein        = "zr_lime:stone",
	clust_scarcity = 9 * 9 * 9,
	clust_num_ores = 12,
	clust_size     = 3,
	y_max          = 31000,
	y_min          = 1025,
})

minetest.register_ore({
	ore_type       = "scatter",
	ore            = "bm_limestone:iron_ore",
	wherein        = "zr_lime:stone",
	clust_scarcity = 7 * 7 * 7,
	clust_num_ores = 5,
	clust_size     = 3,
	y_max          = -128,
	y_min          = -255,
})

minetest.register_ore({
	ore_type       = "scatter",
	ore            = "bm_limestone:iron_ore",
	wherein        = "zr_lime:stone",
	clust_scarcity = 12 * 12 * 12,
	clust_num_ores = 29,
	clust_size     = 5,
	y_max          = -256,
	y_min          = -31000,
})


local S = minetest.get_translator("zr_wood");

zr_sign.register("zr_wood:sign", {
	description = S("Wooden Sign"), 
	tiles = {"zr_wood_sign_wall.png"},
	inventory_image = "zr_wood_sign.png",
	wield_image = "zr_wood_sign.png",
	sounds = zr_wood.sounds,
	groups = {choppy = 2, attached_node = 1, flammable = 2, oddly_breakable_by_hand = 3},
	recipeitem = "group:wood",
})
minetest.register_alias("wood:sign", "zr_wood:sign")

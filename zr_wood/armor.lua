
local S = minetest.get_translator("zr_wood");

-- Boots
simple_armor.register("zr_wood:boots", {
	description = S("Wooden Boots"),
	inventory_image = "zr_wood_boots_item.png",
	armor = {
		slot = "feet",
		groups = { fleshy = 3 }, 
		texture = "zr_wood_boots.png",
	},
})
simple_armor.boots_recipe("zr_wood:boots", "group:wood")
minetest.register_alias("wood:boots", "zr_wood:boots")

-- Chestplate
simple_armor.register("zr_wood:chestplate", {
	description = S("Wooden Boots"),
	inventory_image = "zr_wood_chestplate_item.png",
	armor = {
		slot = "torso",
		groups = { fleshy = 7 }, 
		texture = "zr_wood_chestplate.png",
	},
})
simple_armor.chestplate_recipe("zr_wood:chestplate", "group:wood")
minetest.register_alias("wood:chestplate", "zr_wood:chestplate")

-- Leggings
simple_armor.register("zr_wood:leggings", {
	description = S("Wooden Boots"),
	inventory_image = "zr_wood_leggings_item.png",
	armor = {
		slot = "legs",
		groups = { fleshy = 6 }, 
		texture = "zr_wood_leggings.png",
	},
})
simple_armor.leggings_recipe("zr_wood:leggings", "group:wood")
minetest.register_alias("wood:leggings", "zr_wood:leggings")

-- Helmet
simple_armor.register("zr_wood:helmet", {
	description = S("Wooden Boots"),
	inventory_image = "zr_wood_helmet_item.png",
	armor = {
		slot = "head",
		groups = { fleshy = 4 }, 
		texture = "zr_wood_helmet.png",
	},
})
simple_armor.helmet_recipe("zr_wood:helmet", "group:wood")
minetest.register_alias("wood:helmet", "zr_wood:helmet")


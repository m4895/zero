
local S = minetest.get_translator("zr_shrub")

minetest.register_node("zr_shrub:dry", {
	description = S("Dry Shrub"),
	drawtype = "plantlike",
	waving = 1,
	tiles = {"zr_shrub_dry.png"},
	inventory_image = "zr_shrub_dry.png",
	wield_image = "zr_shrub_dry.png",
	paramtype = "light",
	paramtype2 = "meshoptions",
	place_param2 = 4,
	sunlight_propagates = true,
	walkable = false,
	buildable_to = true,
	groups = {oddly_breakable_by_hand = 3, snappy = 3, flammable = 3, attached_node = 1},
	sounds = zr_wood.leaves_sounds,
	selection_box = {
		type = "fixed",
		fixed = {-6 / 16, -0.5, -6 / 16, 6 / 16, 4 / 16, 6 / 16},
	},
})
minetest.register_alias("shrub:dry","zr_shrub:dry")

local schematic_path = minetest.get_modpath("zr_shrub").."/schematics/"

function zr_shrub.add_to_biome(biome, def)

	local shrub_def = {
		name = biome..":dry_shrub",
		deco_type = "simple",
		place_on = {"zr_sand:desert", "zr_sand:sand", "zr_sand:silver"},
		sidelen = 16,
		noise_params = {
			offset = 0,
			scale = 0.02,
			spread = {x = 200, y = 200, z = 200},
			seed = 329,
			octaves = 3,
			persist = 0.6
		},
		biomes = { biome },
		y_max = 31000,
		y_min = 2,
		decoration = "zr_shrub:dry",
		param2 = 4,
	}

	def = def or {}
	for k, v in pairs(def) do shrub_def[k] = v end

	minetest.register_decoration(shrub_def)
end

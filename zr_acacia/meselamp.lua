
local S = minetest.get_translator("zr_acacia");

zr_mese.register_lampost("zr_acacia:mese_post_light", {
	description = S("Acacia Wood Mese Post Light"),
	texture = "zr_acacia_fence.png",
	material = "zr_acacia:wood",
})

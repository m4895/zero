
zr_bronze = {}

local mod_path = minetest.get_modpath("zr_bronze")
dofile(mod_path.."/bronze.lua")

if (minetest.get_modpath("simple_armor") ~= nil) then
	dofile(mod_path.."/armor.lua")
end

-- bronze
local S = minetest.get_translator("zr_bronze");

local bronze_metal = zr_metal.register_metal("zr_bronze", {
	block = {description = S("Bronze Block"), png = "zr_bronze_block.png"},
	ingot = {description = S("Bronze Ingot"), png = "zr_bronze_ingot.png"},
})
minetest.register_alias("bronze:block", "zr_bronze:block")
minetest.register_alias("bronze:ingot", "zr_bronze:ingot")

minetest.register_craft({
       output = "zr_bronze:ingot 9",
       recipe = {
               {"zr_copper:ingot", "zr_copper:ingot", "zr_copper:ingot"},
               {"zr_copper:ingot", "zr_tin:ingot",    "zr_copper:ingot"},
               {"zr_copper:ingot", "zr_copper:ingot", "zr_copper:ingot"},
       }
})

-- tools 

if(minetest.get_modpath("zr_tools")) ~= nil then

	zr_tools.register_pickaxe("zr_bronze:pick", {
		description = S("Bronze Pickaxe"),
		inventory_image = "zr_bronze_pick.png",
		tool_capabilities = {
			full_punch_interval = 1.0,
			max_drop_level=1,
			groupcaps={
				cracky = {times={[1]=4.50, [2]=1.80, [3]=0.90}, uses=20, maxlevel=2},
			},
			damage_groups = {fleshy=4},
		},
		recipeitem = bronze_metal.ingot,
	})
	minetest.register_alias("bronze:pick", "zr_bronze:pick")
	
	zr_tools.register_shovel("zr_bronze:shovel", {
		description = S("Bronze Shovel"),
		inventory_image = "zr_bronze_shovel.png",
		wield_image = "zr_bronze_shovel.png^[transformR90",
		tool_capabilities = {
			full_punch_interval = 1.1,
			max_drop_level=1,
			groupcaps={
				crumbly = {times={[1]=1.65, [2]=1.05, [3]=0.45}, uses=25, maxlevel=2},
			},
			damage_groups = {fleshy=3},
		},
		recipeitem = bronze_metal.ingot,
	})
	minetest.register_alias("bronze:shovel", "zr_bronze:shovel")

	zr_tools.register_axe("zr_bronze:axe", {
		description = S("Bronze Axe"),
		inventory_image = "zr_bronze_axe.png",
		tool_capabilities = {
			full_punch_interval = 1.0,
			max_drop_level=1,
			groupcaps={
				choppy={times={[1]=2.75, [2]=1.70, [3]=1.15}, uses=20, maxlevel=2},
			},
			damage_groups = {fleshy=4},
		},
		recipeitem = bronze_metal.ingot,
	})
	minetest.register_alias("bronze:axe", "zr_bronze:axe")

	zr_tools.register_sword("zr_bronze:sword", {
		description = S("Bronze Sword"),
		inventory_image = "zr_bronze_sword.png",
		tool_capabilities = {
			full_punch_interval = 0.8,
			max_drop_level=1,
			groupcaps={
				snappy={times={[1]=2.75, [2]=1.30, [3]=0.375}, uses=25, maxlevel=2},
			},
			damage_groups = {fleshy=6},
		},
		recipeitem = bronze_metal.ingot,
	})
	minetest.register_alias("bronze:sword", "zr_bronze:sword")

end


local S = minetest.get_translator("zr_apple");

-- DOOR
zr_door.register("zr_apple:door", {
	tiles = {{ name = "zr_apple_door.png", backface_culling = true}},
	inventory_image = "zr_apple_door_item.png",
	wield_image = "zr_apple_door_item.png",
	description = S("Wooden Door"),
	material = "group:wood"   --TODO change to apple:wood when other wooden doors are implimented
})
minetest.register_alias("apple:door", "zr_apple:door")

-- GATE
zr_door.register_gate("zr_apple:gate", {
	description = S("Apple Wood Fence Gate"),
	texture = "zr_apple_wood.png",
	material = "zr_apple:wood",
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
	sounds = zr_wood.sounds,
})
minetest.register_alias("apple:gate", "zr_apple:gate")

-- TRAPDOOR
zr_door.register_trapdoor("zr_apple:trapdoor", {
	description = S("Wooden Trapdoor"),
	inventory_image = "zr_apple_trapdoor.png",
	wield_image = "zr_apple_trapdoor.png",
	tile_front = "zr_apple_trapdoor.png",
	tile_side = "zr_apple_trapdoor_side.png",
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, door = 1},
})
minetest.register_alias("apple:trapdoor", "zr_apple:trapdoor")

-- TODO change to apple when other trapdoor textures are created
minetest.register_craft({
	output = "zr_apple:trapdoor",
	recipe = {
		{"group:wood", "group:wood", "group:wood"},
		{"group:wood", "group:wood", "group:wood"},
	}
})

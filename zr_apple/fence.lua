
local S = minetest.get_translator("zr_apple");

-- FENCE
zr_fence.register_fence("zr_apple:fence", {                                                                       
    description = S("Apple Wood Fence"),
    texture = "zr_apple_fence.png",
    material = "zr_apple:wood",
    groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
    sounds = zr_wood.sounds,
})
minetest.register_alias("apple:fence", "zr_apple:fence")

zr_fence.register_fence_rail("zr_apple:fence_rail", {                                                                       
    description = S("Apple Wood Fence"),
    texture = "zr_apple_fence_rail.png",
    material = "zr_apple:wood",
    groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
    sounds = zr_wood.sounds,
})
minetest.register_alias("apple:fence_rail", "zr_apple:fence_rail")

minetest.register_craft({
	type = "fuel",
	recipe = "zr_apple:fence",
	burntime = 7,
})

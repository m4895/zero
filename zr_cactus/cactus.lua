local S = minetest.get_translator("zr_cactus")

function zr_cactus.grow(pos, node)
	if node.param2 >= 4 then
		return
	end
	pos.y = pos.y - 1
	if minetest.get_item_group(minetest.get_node(pos).name, "sand") == 0 then
		return
	end
	pos.y = pos.y + 1
	local height = 0
	while node.name == "zr_cactus:cactus" and height < 4 do
		height = height + 1
		pos.y = pos.y + 1
		node = minetest.get_node(pos)
	end
	if height == 4 or node.name ~= "air" then
		return
	end
	if minetest.get_node_light(pos) < 13 then
		return
	end
	minetest.set_node(pos, {name = "zr_cactus:cactus"})
	return true
end

function zr_cactus.grow_large_cactus(pos)
	local path = minetest.get_modpath("zr_cactus") ..
		"/schematics/large_cactus.mts"
	minetest.place_schematic({x = pos.x - 2, y = pos.y - 1, z = pos.z - 2},
		path, "random", nil, false)
end

minetest.register_node("zr_cactus:cactus", {
	description = S("Cactus"),
	tiles = {"zr_cactus_top.png", "zr_cactus_top.png",
		"zr_cactus_side.png"},
	paramtype2 = "facedir",
	groups = {choppy = 3},
	sounds = zr_wood.sounds,
	on_place = minetest.rotate_node,
})
minetest.register_alias("cactus:cactus", "zr_cactus:cactus")
minetest.register_alias("cactus", "zr_cactus:cactus")


minetest.register_node("zr_cactus:large_seedling", {
	description = S("Large Cactus Seedling"),
	drawtype = "plantlike",
	tiles = {"zr_cactus_large_seedling.png"},
	inventory_image = "zr_cactus_large_seedling.png",
	wield_image = "zr_cactus_large_seedling.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	selection_box = {
		type = "fixed",
		fixed = {
			-5 / 16, -0.5, -5 / 16,
			5 / 16, 0.5, 5 / 16
		}
	},
	groups = {choppy = 3, dig_immediate = 3, attached_node = 1},
	sounds = zr_wood.sounds,

	on_place = function(itemstack, placer, pointed_thing)
		itemstack = zr_wood.sapling_on_place(itemstack, placer, pointed_thing,
			"zr_cactus:large_seedling",
			{x = -2, y = -1, z = -2},
			{x = 2, y = 5, z = 2},
			4)

		return itemstack
	end,

	on_construct = function(pos)
		-- Normal cactus farming adds 1 cactus node by ABM,
		-- interval 12s, chance 83.
		-- Consider starting with 5 cactus nodes. We make sure that growing a
		-- large cactus is not a faster way to produce new cactus nodes.
		-- Confirmed by experiment, when farming 5 cacti, on average 1 new
		-- cactus node is added on average every
		-- 83 / 5 = 16.6 intervals = 16.6 * 12 = 199.2s.
		-- Large cactus contains on average 14 cactus nodes.
		-- 14 * 199.2 = 2788.8s.
		-- Set random range to average to 2789s.
		minetest.get_node_timer(pos):start(math.random(1859, 3719))
	end,

	on_timer = function(pos)
		local node_under = minetest.get_node_or_nil(
			{x = pos.x, y = pos.y - 1, z = pos.z})
		if not node_under then
			-- Node under not yet loaded, try later
			minetest.get_node_timer(pos):start(300)
			return
		end

		if minetest.get_item_group(node_under.name, "sand") == 0 then
			-- Seedling dies
			minetest.remove_node(pos)
			return
		end

		local light_level = minetest.get_node_light(pos)
		if not light_level or light_level < 13 then
			-- Too dark for growth, try later in case it's night
			minetest.get_node_timer(pos):start(300)
			return
		end

		minetest.log("action", "A large cactus seedling grows into a large" ..
			"zr_cactus at ".. minetest.pos_to_string(pos))
		zr_cactus.grow_large_cactus(pos)
	end,
})

minetest.register_abm({
	label = "Grow cactus",
	nodenames = {"zr_cactus:cactus"},
	neighbors = {"group:sand"},
	interval = 12,
	chance = 83,
	action = function(...)
		zr_cactus.grow(...)
	end
})


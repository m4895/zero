zr_stone = {};

local mod_path = minetest.get_modpath("zr_stone")
dofile(mod_path.."/stone.lua")
dofile(mod_path.."/alias.lua")

if(minetest.get_modpath("zr_stair")) ~= nil then
	dofile(mod_path.."/stair.lua")
end

if(minetest.get_modpath("zr_tools")) ~= nil then
	dofile(mod_path.."/tools.lua")
end

if(minetest.get_modpath("zr_fence")) ~= nil then
	dofile(mod_path.."/wall.lua")
end

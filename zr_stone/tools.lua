
local S = minetest.get_translator("zr_stone")

-- Stone tools
zr_tools.register_pickaxe("zr_stone:pick", {
    description = S("Stone Pickaxe"),
    inventory_image = "zr_stone_pick.png",
    tool_capabilities = {
        full_punch_interval = 1.3,
        max_drop_level=0,
        groupcaps={
            cracky = {times={[2]=2.0, [3]=1.00}, uses=20, maxlevel=1},
        },
        damage_groups = {fleshy=3},
    },
	recipeitem = "zr_stone:cobble"
})
minetest.register_alias("stone:pick","zr_stone:pick")

zr_tools.register_axe("zr_stone:axe", {
    description = S("Stone Axe"),
    inventory_image = "zr_stone_axe.png",
    tool_capabilities = {
        full_punch_interval = 1.2,
        max_drop_level=0,
        groupcaps={
            choppy={times={[1]=3.00, [2]=2.00, [3]=1.30}, uses=20, maxlevel=1},
        },
        damage_groups = {fleshy=3},
    },
	recipeitem = "zr_stone:cobble"
})
minetest.register_alias("stone:axe","zr_stone:axe")

zr_tools.register_shovel("zr_stone:shovel", {
    description = S("Stone Shovel"),
    inventory_image = "zr_stone_shovel.png",
    tool_capabilities = {
        full_punch_interval = 1.2,
        max_drop_level=0,
        groupcaps={
			crumbly = {times={[1]=1.80, [2]=1.20, [3]=0.50}, uses=20, maxlevel=1},

        },
        damage_groups = {fleshy=3},
    },
	recipeitem = "zr_stone:cobble"
})
minetest.register_alias("stone:shovel","zr_stone:shovel")

zr_tools.register_sword("zr_stone:sword", {
    description = S("Stone Sword"),
    inventory_image = "zr_stone_sword.png",
    tool_capabilities = {
        full_punch_interval = 1.2,
        max_drop_level=0,
        groupcaps={
            snappy={times={[2]=1.4, [3]=0.40}, uses=20, maxlevel=1},
        },
        damage_groups = {fleshy=4},
    },
	recipeitem = "zr_stone:cobble"
})
minetest.register_alias("stone:sword","zr_stone:sword")

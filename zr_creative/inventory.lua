
local creative_form = [[
	formspec_version[6]
	size[16,15]
	bgcolor[;true;#0000002F]
	background9[ 0,0 ; 16.75,14 ;zr_creative_bg.png;false;10]
	list[current_player;main; 3.5,1 ; 8,1 ;]
	list[detached:zr_creative_%s;main; 1,3 ; 12,8 ;]
	button[ 1,2.5 ; 0.5,0.5 ;zr_creative_btn_prev;<]
	button[ 15.25,2.5 ; 0.5,0.5 ;zr_creative_btn_next;>]
	field[ 6,2.5 ; 4.5,0.5 ;zr_creative_search;;]
	field_close_on_enter[zr_creative_search;false]
	listring[detached:zr_creative_%s;main]
	listring[current_player;main]
	listring[detached:zr_creative_trash;main]
]]

local trash = minetest.create_detached_inventory("zr_creative_trash", {
	on_put = function(inv, listname, index, stack, player)
		inv:set_list("main", {})
	end,
})
trash:set_size("main",1)

local get_form = function(player) 
	local p_name = player:get_player_name()
	local form = creative_form:format(p_name, p_name)
	return form
end

local all_items = {}
local player_data = {}

minetest.register_on_mods_loaded(function()
	for name, def in pairs(minetest.registered_items) do
		if name ~= "" and not def.groups.not_in_creative_inventory then
			table.insert(all_items,name)
		end
	end
	table.sort(all_items)
end)

local PAGE_SIZE = 8*12

local get_list = function(filter_text, offset)
	local p_list = {}

	for i, v in ipairs(all_items) do
		if not offset or i > offset then
			if not filter_text or v:find(filter_text) then
				table.insert(p_list, ItemStack(v))
			end
		end
	end
	return p_list
end

local new_picklist = function(player)
	local p_name = player:get_player_name()

	local inv = minetest.create_detached_inventory("zr_creative_" .. p_name, {
		allow_take = function(inv, listname, index, stack, player2)
			local p2_name = player2:get_player_name()
			if not minetest.is_creative_enabled(p2_name) then return 0 end
			return -1
		end,
		allow_put = function() return 0 end,
	}, p_name)

	inv:set_list("main", get_list())
	player_data[p_name] = {i = inv, page = 0}
end


minetest.register_on_player_receive_fields(function(player, formname, fields)
	local p_name = player:get_player_name()
	local p_data = player_data[p_name]
	if not p_data then return end
	local inv = player_data[p_name].i

	if fields.key_enter_field == "zr_creative_search" then 
		inv:set_list("main", get_list(fields.zr_creative_search))

	elseif fields.zr_creative_btn_next then
		local old_page = player_data[p_name].page
		local next_item = inv:get_list("main")[PAGE_SIZE+1]

		if not next_item or next_item:is_empty() then return end

		player_data[p_name].page = old_page+1
		inv:set_list("main", get_list(nil, (old_page+1) * PAGE_SIZE))

	elseif fields.zr_creative_btn_prev then
		local inv_size = inv:get_size("main")
		local old_page = player_data[p_name].page
		if old_page == 0 then return end

		player_data[p_name].page = old_page-1
		inv:set_list("main", get_list(nil, (old_page-1) * PAGE_SIZE))
	elseif fields.quit then
		inv:set_list("main", get_list())
		player_data[p_name].page = 0
	end
end)

zr_creative.set_inv = function(player)
	new_picklist(player)
	player:set_inventory_formspec(get_form(player))
	player:get_inventory():set_size("main",8)
end


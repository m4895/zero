
local inv_form = [[
	formspec_version[6]
	size[15,11]
	bgcolor[;true;#0000002F]
	background9[ 1.75,0.75 ; 5.25,4 ;zr_inv_bg2.png;false;10]
	background9[ 8,3.25 ; 1.5,1.5 ;zr_inv_bg.png;false;10]
	background9[ 0.75,5 ; 10.25,5.25 ;zr_inv_bg.png;false;10]
	background9[ 0.85,5.1 ; 10.05,1.3 ;zr_inv_bg3.png;false;10]
	list[current_player;craft;2,1;3,3;]
	list[current_player;craftpreview; 5.75,2.25 ; 1,1;]
	list[current_player;main;1,5.25;8,1;]
	list[current_player;main;1,6.5;8,3;8]
	list[detached:zr_inv_trash;main; 8.25,3.5 ; 1,1;]
	image[ 5.55, 3.4 ; 1,1 ;zr_inv_arrow_up.png]
	image[ 5.55, 1.1 ; 1,1 ;zr_inv_arrow_up.png^[transformFY]
	image[ 8.3, 3.55 ; 0.9,0.9 ;zr_inv_trash.png]
	listring[current_player;main]
	listring[current_player;craft]
]]

local trash = minetest.create_detached_inventory("zr_inv_trash", {
	on_put = function(inv, listname, index, stack, player)
		minetest.sound_play("zr_inv_trash")
		inv:set_list("main", {})
	end,
})
trash:set_size("main",1)

-- default inventory function (may be overriden)
zr_inv.set_inv = function(player)
	player:set_inventory_formspec(inv_form)
end

minetest.register_on_joinplayer(function(player)
	zr_inv.set_inv(player)
end)

minetest.register_on_craft(function()
	minetest.sound_play("zr_inv_craft")
end)

local hand_digs

if minetest.is_creative_enabled("") then
	hand_digs = {times={[1]=0.2,[2]=0.2,[3]=0.2,[4] = 0.2}, uses=0} 
else
	hand_digs = {times={[1]=15,[2]=3.00,[3]=1.70,[4] = 0.7}, uses=0} 
end

local hand_def = {
    type = 'none',
    wield_image = 'zr_player_hand.png',
    wield_scale = {x = 0.5, y = 1, z = 4},
    tool_capabilities = {
        full_punch_interval = 0.9,
        max_drop_level = 0,
        groupcaps = {
            oddly_breakable_by_hand = hand_digs,
        },
        damage_groups = {fleshy = 1},
    },
}

minetest.register_craftitem(':', hand_def)


local mc_64x32 = {
	candypop = {"zr_player_skin_candypop.png"},
	castaway = {"zr_player_skin_castaway.png"},
	steve = {"zr_player_skin_character.png"},
	countrygirl = {"zr_player_skin_countrygirl.png"},
	cthulhu = {"zr_player_skin_cthulhu.png"},
	dj = {"zr_player_skin_dj.png"},
	femalepirate = {"zr_player_skin_femalepirate.png"},
	firemage = {"zr_player_skin_firemage.png"},
	firemonster = {"zr_player_skin_firemonster.png"},
	frostgirl = {"zr_player_skin_frostgirl.png"},
	girl = {"zr_player_skin_girl.png"},
	greenknight = {"zr_player_skin_greenknight.png"},
	grimreaper = {"zr_player_skin_grimreaper.png"},
	iceprincess = {"zr_player_skin_iceprincess.png"},
	lisa = {"zr_player_skin_lisa.png"},
	nightsky = {"zr_player_skin_nightsky.png"},
	pirate = {"zr_player_skin_pirate.png"},
	steampunksuit = {"zr_player_skin_steampunksuit.png"},
	Summer_by_lizzie = {"zr_player_skin_Summer_by_lizzie.png"},
	Summer = {"zr_player_skin_Summer.png"},
	vampirehunter = {"zr_player_skin_vampirehunter.png"},
	yeti = {"zr_player_skin_yeti.png"},
}

local mc_64x64 = {
	amongusblue = {"zr_player_skin_amongusblue.png"},
	arabman = {"zr_player_skin_arabman.png"},
	atsukokagari = {"zr_player_skin_atsukokagari.png"},
	baker = {"zr_player_skin_baker.png"},
	bellatrix = {"zr_player_skin_bellatrix.png"},
	blackvest = {"zr_player_skin_blackvest.png"},
	castawaygirl = {"zr_player_skin_castawaygirl.png"},
	chucky = {"zr_player_skin_chucky.png"},
	coralinejones = {"zr_player_skin_coralinejones.png"},
	death = {"zr_player_skin_death.png"},
	demongirl = {"zr_player_skin_demongirl.png"},
	elvengirl = {"zr_player_skin_elvengirl.png"},
	foresthunter = {"zr_player_skin_foresthunter.png"},
	greenchristmassweater = {"zr_player_skin_greenchristmassweater.png"},
	Hercules = {"zr_player_skin_Hercules.png"},
	jessa = {"zr_player_skin_jessa.png"},
	kaidoushun = {"zr_player_skin_kaidoushun.png"},
	monkey = {"zr_player_skin_monkey.png"},
	moonstar = {"zr_player_skin_moonstar.png"},
	rainboots = {"zr_player_skin_rainboots.png"},
	rainbowhair = {"zr_player_skin_rainbowhair.png"},
	redshadedhoodie = {"zr_player_skin_redshadedhoodie.png"},
	snowmansweater = {"zr_player_skin_snowmansweater.png"},
	springisintheair = {"zr_player_skin_springisintheair.png"},
	tattooman = {"zr_player_skin_tattooman.png"},
	torven = {"zr_player_skin_torven.png"},
	walterwhite = {"zr_player_skin_walterwhite.png"},
	wanderer = {"zr_player_skin_wanderer.png"},
	whitehood = {"zr_player_skin_whitehood.png"},
}

local mc_64x64slim = {
	beegirl = {"zr_player_skin_beegirl.png"},
	cupcakegirl = {"zr_player_skin_cupcakegirl.png"},
	gymgirl = {"zr_player_skin_gymgirl.png"},
	pinkbluerabbit = {"zr_player_skin_pinkbluerabbit.png"},
	sayuri = {"zr_player_skin_sayuri.png"},
}

-- Assign skins to models and set default skin for each model
local mc = zr_player.models.mc
mc.skins = mc_64x64
mc.skins.default = mc.skins.walterwhite

local slim = zr_player.models.slim
slim.skins = mc_64x64slim
slim.skins.default = slim.skins.beegirl

local classic = zr_player.models.classic
classic.skins = mc_64x32
classic.skins.default = classic.skins.steve

local armor3d = zr_player.models.armor3d
armor3d.skins = {}
for k, v in pairs(mc_64x32) do
	armor3d.skins[k] = {v[1], "zr_player_trans.png", "zr_player_trans.png"}
end
armor3d.skins.default = armor3d.skins.steve



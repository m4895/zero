zr_player.models={

	zero={
		props={
			mesh = "player.b3d",
			visual = "mesh",
			visual_size = {x=1, y=1},
			collisionbox = {-0.3, 0.0, -0.3, 0.3, 1.7, 0.3},
			stepheight = 0.6,
			eye_height = 1.47,
		},

		stand = {frame_range={x=1, y=29}, frame_rate=30},
		sit = {frame_range={x=31, y=49}, frame_rate=30},
		lie = {frame_range={x=51, y=51}, frame_rate=30},
		run = {frame_range={x=52, y=71}, frame_rate=30},
		sneak = {frame_range={x=73, y=73}, frame_rate=30},
		sneak_walk = {frame_range={x=73, y=93}, frame_rate=30},
		dig = {frame_range={x=94, y=108}, frame_rate=30},
		run_dig = {frame_range={x=110, y=129}, frame_rate=30},
		sneak_dig = {frame_range={x=131, y=139}, frame_rate=30},
		sneak_walk_dig = {frame_range={x=141, y=160}, frame_rate=30},
		swim = {frame_range={x=163,y=182}, frame_rate=30},

		wield = {
			bone = "ArmR",
			pos = vector.new(-0.1,1.2,0),
			rot = vector.new(0,-90,0),
			size = {x = 0.04, y = 0.04, z = 0.04},
		},
	},

	slim={
		props={
			mesh = "slim.b3d",
			visual = "mesh",
			visual_size = {x=1, y=1},
			collisionbox = {-0.3, 0.0, -0.3, 0.3, 1.7, 0.3},
			stepheight = 0.6,
			eye_height = 1.47,
		},

		stand = {frame_range={x=1, y=29}, frame_rate=30},
		sit = {frame_range={x=31, y=49}, frame_rate=30},
		lie = {frame_range={x=51, y=51}, frame_rate=30},
		run = {frame_range={x=52, y=71}, frame_rate=30},
		sneak = {frame_range={x=73, y=73}, frame_rate=30},
		sneak_walk = {frame_range={x=73, y=93}, frame_rate=30},
		dig = {frame_range={x=94, y=108}, frame_rate=30},
		run_dig = {frame_range={x=110, y=129}, frame_rate=30},
		sneak_dig = {frame_range={x=131, y=139}, frame_rate=30},
		sneak_walk_dig = {frame_range={x=141, y=160}, frame_rate=30},
		swim = {frame_range={x=163,y=182}, frame_rate=30},

		wield = {
			bone = "ArmR",
			pos = vector.new(-0.1,1.2,0),
			rot = vector.new(0,-90,0),
			size = {x = 0.04, y = 0.04, z = 0.04},
		},
	},

	classic={
		props={
			mesh = "character.b3d",
			visual = "mesh",
			visual_size = {x=1, y=1},
			collisionbox = {-0.3, 0.0, -0.3, 0.3, 1.7, 0.3},
			stepheight = 0.6,
			eye_height = 1.47,
		},

		stand     = {frame_range={x = 0,   y = 79}, frame_rate=30}, 
		lie       = {frame_range={x = 162, y = 166},frame_rate=30},
		run      = {frame_range={x = 168, y = 187},frame_rate=30},
		dig      = {frame_range={x = 189, y = 198},frame_rate=30},
		run_dig = {frame_range={x = 200, y = 219},frame_rate=30},
		sit       = {frame_range={x = 81,  y = 160},frame_rate=30},
		sneak_dig = {frame_range={x = 189, y = 190},frame_rate=15},
		sneak_walk_dig = {frame_range={x = 189, y = 198},frame_rate=15},

		wield = {
			bone = "Arm_Right",
			pos = vector.new(-0.6,6.5,0),
			rot = vector.new(0,-90,0),
			size = {x = 0.25, y = 0.25, z = 0.25},
		},
	},

	armor3d= {
		props={
			mesh = "3d_armor_character.b3d",
		},

		stand = {frame_range={x=0, y=79}, frame_rate=30},
		lie = {frame_range={x=162, y=166}, frame_rate=30},
		run = {frame_range={x=168, y=187}, frame_rate=30},
		dig = {frame_range={x=189, y=198}, frame_rate=30},
		run_dig = {frame_range={x=200, y=219}, frame_rate=30},
		sit = {frame_range={x=81, y=160}, frame_rate=30},
		sneak = {frame_range={x=0, y=79}, frame_rate=30}, -- stand
		sneak_walk = {frame_range={x=168, y=187}, frame_rate=15}, -- run (half speed)
		sneak_dig = {frame_range={x=189, y=198}, frame_rate=15}, -- dig (half speed)
		sneak_walk_dig = {frame_range={x=200, y=219}, frame_rate=15}, -- run_dig (half speed)

		wield = {
			bone = "Arm_Right",
			pos = vector.new(-0.6,6.5,0),
			rot = vector.new(0,-90,0),
			size = {x = 0.25, y = 0.25, z = 0.25},
		},
	},
}

-- set default (alias)
zr_player.models.default=zr_player.models.zero
zr_player.models.mc = zr_player.models.zero
